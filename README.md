Magento 2 API Calculator

This extension adds REST API _/V1/api/rce/calculator_ endpoint and new GraphQL _calculate_ query support. It supports two operands, five operators, precision and formatting configuration. 

To get more information access /swagger route with your browser and /graphql with client like ChromeiQL.

**Note**: extension was developed for Magento 2.3.1+, for demonstration purposes only.