<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Api;

interface CalculatorServiceInterface
{

    /**
     * Calculator service interface
     *
     * @param float $left
     * @param float $right
     * @param string $operator
     * @param int|null $precision
     * @param string|null $formatter
     * @return CalculationResultInterface
     */
    public function calculate(
        float $left,
        float $right,
        string $operator,
        ?int $precision = null,
        ?string $formatter = null
    ): CalculationResultInterface;
}
