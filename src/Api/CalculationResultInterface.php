<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Api;

interface CalculationResultInterface
{

    /**
     * Get result
     *
     * @return float
     */
    public function getResult(): float;

    /**
     * Set result
     *
     * @param float $result
     * @return void
     */
    public function setResult(float $result): void;
}
