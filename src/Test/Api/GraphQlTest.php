<?php

declare(strict_types=1);

namespace Magento\GraphQl\CatalogInventory;

use Magento\TestFramework\TestCase\GraphQlAbstract;

class GraphQlTest extends GraphQlAbstract
{
    public function testQueryProductStockStatusInStock(): void
    {
        $query = <<<QUERY
        {
             calculate (left: 2, right:3, operator: "divide", precision: 4) {
                result
             }
        }
QUERY;

        $response = $this->graphQlQuery($query);

        $this->assertEquals(0.6667, $response['calculate']['result']);
    }
}
