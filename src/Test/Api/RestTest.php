<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Api;

use Magento\Framework\Webapi\Rest\Request;
use Magento\TestFramework\TestCase\WebapiAbstract;

class RestTest extends WebapiAbstract
{
    public function testCalculate(): void
    {
        $serviceInfo = [
            'rest' => [
                'resourcePath' => '/V1/api/rce/calculator',
                'httpMethod' => Request::HTTP_METHOD_POST,
            ]
        ];

        $requestData = [
            'left' => 2,
            'right' => 3,
            'operator' => 'divide',
            'precision' => 4
        ];

        $data = $this->_webApiCall($serviceInfo, $requestData);
        $this->assertEquals(0.6667, $data['result']);
    }
}
