<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Cache\Type;

use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\Cache\Frontend\Decorator\TagScope;

class Result extends TagScope
{
    private const TYPE_IDENTIFIER = 'apicalculator_result';
    private const CACHE_TAG = 'APICALCULATOR_RESULT';

    public function __construct(FrontendPool $cacheFrontendPool)
    {
        parent::__construct($cacheFrontendPool->get(self::TYPE_IDENTIFIER), self::CACHE_TAG);
    }
}
