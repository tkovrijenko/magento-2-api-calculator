<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Cache;

use JsonException;
use TKovrijenko\ApiCalculator\Model\Cache\Type\Result as Cache;
use TKovrijenko\ApiCalculator\Model\Logger;

class Handler
{
    private const CACHE_ID_SEPARATOR = '|';

    private const ERROR_JSON_DECODING = 'Error during cache JSON decoding';
    private const ERROR_JSON_ENCODING = 'Error during cache JSON encoding';

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Handler constructor.
     * @param Cache $cache
     */
    public function __construct(Cache $cache, Logger $logger)
    {
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @param array $args
     * @return string
     */
    private function getCacheId(array $args): string
    {
        return implode(self::CACHE_ID_SEPARATOR, $args);
    }

    /**
     * @param array $args
     * @return float|null
     */
    public function getFromCache(array $args): ?float
    {
        $cacheId = $this->getCacheId($args);

        if ($this->cache->test($cacheId)) {
            $serializedData = $this->cache->load($cacheId);
            try {
                return json_decode($serializedData, true, 1, JSON_THROW_ON_ERROR);
            } catch (JsonException $e) {
                $this->logger->error(
                    self::ERROR_JSON_DECODING,
                    [
                        'data (first 128 symbols)' => substr($serializedData, 0, 128)
                    ] + compact('cacheId')
                );
            }
        }

        return null;
    }

    /**
     * @param $data
     * @param $args
     * @return bool
     */
    public function saveToCache($data, $args): bool
    {
        try {
            return $this->cache->save(json_encode($data, JSON_THROW_ON_ERROR), self::getCacheId($args));
        } catch (JsonException $e) {
            $this->logger->error(self::ERROR_JSON_ENCODING, compact('data'));
        }

        return false;
    }
}
