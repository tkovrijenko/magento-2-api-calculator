<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model;

use TKovrijenko\ApiCalculator\Api\CalculationResultInterface;
use TKovrijenko\ApiCalculator\Api\CalculatorServiceInterface;

class CalculatorService implements CalculatorServiceInterface
{
    private $calculatorAdapter;
    private $calculationResultFactory;

    public function __construct(
        CalculatorAdapter $calculatorAdapter,
        CalculationResultFactory $calculationResultFactory
    ) {
        $this->calculatorAdapter = $calculatorAdapter;
        $this->calculationResultFactory = $calculationResultFactory;
    }

    public function calculate(
        float $left,
        float $right,
        string $operator,
        ?int $precision = null,
        ?string $formatter = null
    ): CalculationResultInterface {
        $resultData = $this->calculatorAdapter->calculate($left, $right, $operator, $precision, $formatter);

        $resultObject = $this->calculationResultFactory->create();
        $resultObject->setResult($resultData['result']);

        return $resultObject;
    }
}
