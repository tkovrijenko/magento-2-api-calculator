<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model;

use TKovrijenko\ApiCalculator\Api\CalculationResultInterface;

class CalculationResult implements CalculationResultInterface
{
    /**
     * @var float
     */
    private $result;

    public function getResult(): float
    {
        return $this->result;
    }

    public function setResult(float $result): void
    {
        $this->result = $result;
    }
}
