<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Resolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use TKovrijenko\ApiCalculator\Model\CalculatorAdapter;

class CalculationResult implements ResolverInterface
{
    private $calculatorAdapter;

    public function __construct(CalculatorAdapter $calculatorAdapter)
    {
        $this->calculatorAdapter = $calculatorAdapter;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        try {
            $result = $this->calculatorAdapter->calculate(
                $args['left'],
                $args['right'],
                $args['operator'],
                $args['precision'] ?? null,
                $args['formatter'] ?? null
            );
        } catch (LocalizedException $e) {
            if ($e->getCode() === CalculatorAdapter::ERROR_CODE_WRONG_INPUT) {
                throw new GraphQlInputException(__($e->getRawMessage(), $e->getParameters()));
            }

            throw $e;
        }

        return $result;
    }
}
