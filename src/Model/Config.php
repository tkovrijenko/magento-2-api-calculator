<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * API Calculator configuration model
 */
class Config
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Default formatter option path
     */
    private const PATH_DEFAULT_FORMATTER = 'api_calculator/defaults/default_formatter';

    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get default formatter
     *
     * @return string
     */
    public function getDefaultFormatter(): string
    {
        return $this->scopeConfig->getValue(self::PATH_DEFAULT_FORMATTER);
    }
}
