<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use TKovrijenko\ApiCalculator\Model\Calculator\Formatter\FormatterProvider;

class Formatter implements OptionSourceInterface
{
    private $formatterProvider;

    public function __construct(FormatterProvider $formatterProvider)
    {
        $this->formatterProvider = $formatterProvider;
    }

    public function toOptionArray()
    {
        $list = $this->formatterProvider->getList();
        return array_map(
            static function (string $class, string $code) {
                return ['value' => $code, 'label' => $class::LABEL];
            },
            $list,
            array_keys($list)
        );
    }
}
