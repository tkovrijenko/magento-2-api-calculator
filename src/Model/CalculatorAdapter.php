<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model;

use Magento\Framework\Exception\LocalizedException;
use TKovrijenko\ApiCalculator\Model\Cache\Handler as CacheHandler;
use TKovrijenko\ApiCalculator\Model\Calculator\CalculatorInterfaceFactory;
use TKovrijenko\ApiCalculator\Model\Calculator\Formatter\FormatterProvider;

class CalculatorAdapter
{
    private const OPERATOR_ADD = 'add';
    private const OPERATOR_SUBTRACT = 'subtract';
    private const OPERATOR_MULTIPLY = 'multiply';
    private const OPERATOR_DIVIDE = 'divide';
    private const OPERATOR_POWER = 'power';

    private const OPERATORS_MAP = [
        self::OPERATOR_ADD => 'summarize',
        self::OPERATOR_SUBTRACT => 'subtract',
        self::OPERATOR_MULTIPLY => 'multiply',
        self::OPERATOR_DIVIDE => 'divide',
        self::OPERATOR_POWER => 'pow'
    ];

    public const ERROR_CODE_WRONG_INPUT = 1;

    /**
     * @var CalculatorInterfaceFactory
     */
    private $calculatorFactory;

    /**
     * @var FormatterProvider
     */
    private $formatterProvider;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var CacheHandler
     */
    private $cacheHandler;

    /**
     * CalculatorAdapter constructor.
     * @param CalculatorInterfaceFactory $calculatorFactory
     * @param FormatterProvider $formatterProvider
     * @param Config $config
     * @param CacheHandler $cacheHandler
     */
    public function __construct(
        CalculatorInterfaceFactory $calculatorFactory,
        FormatterProvider $formatterProvider,
        Config $config,
        CacheHandler $cacheHandler
    ) {
        $this->calculatorFactory = $calculatorFactory;
        $this->formatterProvider = $formatterProvider;
        $this->config = $config;
        $this->cacheHandler = $cacheHandler;
    }

    /**
     * @param float $left
     * @param float $right
     * @param string $operator
     * @param int|null $precision
     * @param string|null $formatter
     * @return array
     * @throws LocalizedException
     */
    public function calculate(
        float $left,
        float $right,
        string $operator,
        ?int $precision = null,
        ?string $formatter = null
    ): array {
        $args = func_get_args();
        $result = $this->cacheHandler->getFromCache($args);

        if ($result === null) {
            $result = $this->doCalculate($left, $right, $operator, $precision, $formatter);
            $this->cacheHandler->saveToCache($result, $args);
        }

        return compact('result');
    }

    /**
     * @param float $left
     * @param float $right
     * @param string $operator
     * @param int|null $precision
     * @param string|null $formatter
     * @return mixed
     * @throws LocalizedException
     */
    private function doCalculate(
        float $left,
        float $right,
        string $operator,
        ?int $precision = null,
        ?string $formatter = null
    ) {
        if (!$this->isOperatorAllowed($operator)) {
            throw new LocalizedException(
                __('Wrong operator. Allowed: ' . $this->getAllowedOperatorsList()),
                null,
                self::ERROR_CODE_WRONG_INPUT
            );
        }

        if ($formatter === null) {
            $formatter = $this->config->getDefaultFormatter();
        }

        $formatterInstance = $this->formatterProvider->get($formatter);

        $calculator = $this->calculatorFactory->create(['formatter' => $formatterInstance]);

        if ($precision !== null) {
            $calculator->setPrecision($precision);
        }

        return $calculator->{self::OPERATORS_MAP[$operator]}($left, $right);
    }

    /**
     * @param string $operator
     * @return bool
     */
    private function isOperatorAllowed(string $operator): bool
    {
        return \array_key_exists($operator, self::OPERATORS_MAP);
    }

    /**
     * @return string
     */
    private function getAllowedOperatorsList(): string
    {
        return \implode(', ', array_keys(self::OPERATORS_MAP));
    }
}
