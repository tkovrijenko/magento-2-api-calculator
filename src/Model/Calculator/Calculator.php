<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Calculator;

use TKovrijenko\ApiCalculator\Model\Calculator\Formatter\FormatterInterface;

class Calculator implements CalculatorInterface
{
    private const DEFAULT_PRECISION = 2;

    /**
     * @var int
     */
    private $precision = self::DEFAULT_PRECISION;

    /**
     * @var FormatterInterface
     */
    private $formatter;

    /**
     * Calculator constructor.
     * @param FormatterInterface $formatter
     */
    public function __construct(FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * @param int $precision
     */
    public function setPrecision(int $precision): void
    {
        $this->precision = $precision;
    }

    /**
     * @param $value1
     * @param $value2
     * @param mixed ...$values
     * @return float
     */
    public function summarize($value1, $value2, ...$values): float
    {
        $arr = array_merge([$value1, $value2], $values);
        return $this->formatValue(array_sum($arr));
    }

    /**
     * @param float $left
     * @param float $right
     * @return float
     */
    public function subtract(float $left, float $right): float
    {
        return $this->formatValue($left - $right);
    }

    /**
     * @param $value1
     * @param $value2
     * @param mixed ...$values
     * @return float
     */
    public function multiply($value1, $value2, ...$values): float
    {
        $arr = array_merge([$value1, $value2], $values);
        return $this->formatValue(array_product($arr));
    }

    /**
     * @param float $left
     * @param float $right
     * @return float
     */
    public function divide(float $left, float $right): float
    {
        return $this->formatValue($left / $right);
    }

    /**
     * @param float $base
     * @param float $exp
     * @return float
     */
    public function pow(float $base, float $exp): float
    {
        return $this->formatValue($base ** $exp);
    }

    /**
     * @param $value
     * @return float
     */
    private function formatValue($value): float
    {
        return $this->formatter->format($value, $this->precision);
    }
}
