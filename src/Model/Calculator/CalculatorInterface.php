<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Calculator;

interface CalculatorInterface
{

    /**
     * @param int $precision
     */
    public function setPrecision(int $precision): void;

    /**
     * @param $value1
     * @param $value2
     * @param mixed ...$values
     * @return float
     */
    public function summarize($value1, $value2, ...$values): float;

    /**
     * @param float $left
     * @param float $right
     * @return float
     */
    public function subtract(float $left, float $right): float;

    /**
     * @param $value1
     * @param $value2
     * @param mixed ...$values
     * @return float
     */
    public function multiply($value1, $value2, ...$values): float;

    /**
     * @param float $left
     * @param float $right
     * @return float
     */
    public function divide(float $left, float $right): float;

    /**
     * @param float $base
     * @param float $exp
     * @return float
     */
    public function pow(float $base, float $exp): float;
}
