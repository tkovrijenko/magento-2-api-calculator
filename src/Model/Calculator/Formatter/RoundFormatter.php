<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Calculator\Formatter;

class RoundFormatter implements FormatterInterface
{
    public const CODE = 'round';

    public const LABEL = 'Round';

    public function format($value, int $precision): float
    {
        return round($value, $precision);
    }
}
