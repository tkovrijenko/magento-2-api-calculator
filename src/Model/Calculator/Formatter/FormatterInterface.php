<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Calculator\Formatter;

interface FormatterInterface
{
    public function format($value, int $precision): float;
}
