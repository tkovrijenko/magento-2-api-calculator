<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Calculator\Formatter;

/**
 *  Leaves N significant digits
 */
class SignificantFormatter implements FormatterInterface
{
    public const CODE = 'significant';

    public const LABEL = 'Significant';

    public function format($value, int $precision): float
    {
        return round($value, (int)($precision - 1 - floor(log10($value))));
    }
}
