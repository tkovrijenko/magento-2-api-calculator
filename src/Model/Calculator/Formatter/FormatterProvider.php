<?php

declare(strict_types=1);

namespace TKovrijenko\ApiCalculator\Model\Calculator\Formatter;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;

class FormatterProvider
{
    /**
     * @var array
     */
    private $registered = [];

    /**
     *
     */
    private const ERROR_WRONG_FORMATTER_CLASS = 'Class must implement ' . FormatterInterface::class;

    /**
     * FormatterProvider constructor.
     * @param array $list
     */
    public function __construct(array $list = [])
    {
        $this->registerList($list);
    }

    /**
     * @param array $list
     */
    public function registerList(array $list): void
    {
        foreach ($list as $class) {
            $this->register($class);
        }
    }

    /**
     * @param string $class
     */
    public function register(string $class): void
    {
        if (!is_a($class, FormatterInterface::class, true)) {
            throw new \InvalidArgumentException(self::ERROR_WRONG_FORMATTER_CLASS);
        }

        $this->registered[$class::CODE] = $class;
    }

    /**
     * @param string $name
     * @return FormatterInterface
     * @throws LocalizedException
     */
    public function get(string $name): FormatterInterface
    {
        if (!array_key_exists($name, $this->registered)) {
            $allowedFormatterCodes = implode(', ', array_keys($this->registered));
            throw new LocalizedException(__('Wrong formatter code. Allowed: %1', $allowedFormatterCodes));
        }

        return ObjectManager::getInstance()->get($this->registered[$name]);
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->registered;
    }
}
